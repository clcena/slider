<?php
		$files = array("aguila","caracol","cebras","gorila","mono","oso","papagayo","perro","pez");
		$directorio = "../images/"; 
		$extension = ".jpg";
		$images = Array();
		for ($i=0; $i<count($files); $i++) { 
			$images[$i] = $directorio.$files[$i].$extension;
		}
?>
<div id="slider">
	<ul id="slider-list">
		<li class="actual"><img src="<?= $images[0]?>" alt="<?= $files[0]?>"/></li>
		<li><img src="<?= $images[1]?>" alt="<?= $files[1]?>"/></li>
		<li><img src="<?= $images[2]?>" alt="<?= $files[2]?>"/></li>
		<li><img src="<?= $images[3]?>" alt="<?= $files[3]?>"/></li>
		<li><img src="<?= $images[4]?>" alt="<?= $files[4]?>"/></li>
		<li><img src="<?= $images[5]?>" alt="<?= $files[5]?>"/></li>
	</ul>
	<div id="slider-console">
		<a id="prev" href="">prev</a>
		<div id="slider-pages"></div>
		<a id="next" href="">next</a>
		<span> | </span>
		<a id="play" href="">play</a>
	</div>	
</div>