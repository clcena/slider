$(function(){
	//objeto slider
	var slider  = {
		id					: "#slider",
		width       : 800,
		height      : 400,
		actual      : 0,
		wrap_list   : "#slider-list",
		wrap_pages  : "#slider-pages",
		li_number	: $("#slider-list").find("li").length,
		go          : function(where)
					{
							if (where == 'next'){
									if(slider.actual < slider.li_number - 1){	slider.actual = slider.actual * 1 + 1;	}
									else{																			slider.actual = 0;											}
							}
							else if (where == 'prev'){
									if(slider.actual > 0){	slider.actual = slider.actual - 1;		}
									else{										slider.actual = slider.li_number - 1;	}
							}
							else{
									slider.actual = where;
							}
							//$("#slider-list li").removeClass("actual");
							var li = $("#slider-list").find("li"); 
							var a  = $("#slider-pages").find("a");
							$(li).removeClass("actual");
							$(a).removeClass("nro_actual");

							for (var i = 0; i < slider.li_number; i++)
							{
								if(i == slider.actual){
									//alert(slider.actual);
									$(li[i]).addClass("actual");
									$(a[i]).addClass("nro_actual");
								}
							}
					},
		play 		: function()
					{
						setInterval(function () {
							slider.go('next');
						},3*1000);
					}							
	};
	
	$(slider.wrap_list).css({"width" : slider.width  + "px" , "height": slider.height + "px"}); // #slider
	$(slider.id + " img").css({"width" : slider.width  + "px"});																// #slider img


  var sliderPages = $(slider.wrap_pages);
  var li = $("#slider-list").find("li");
  var nro_pagina;
	for (var i = 0; i < slider.li_number; i++)
	{
		if(i == 0){
			nro_pagina = "<a href='#' class='nro_actual' rel='"+i+"' title='"+$(li[i]).find('img').attr("alt")+"'>"+ parseInt(i + 1) + "</a>";
		}
		else{
			nro_pagina = "<a href='#' rel='"+i+"' title='"+$(li[i]).find('img').attr("alt")+"'>"+ parseInt(i + 1) + "</a>";
		}
		$(sliderPages).append(nro_pagina);
	}
	 

	$(slider.id + " #next").click(function (e) {
			e.preventDefault();
			slider.go('next');
		});
	$(slider.id + " #prev").click(function (e) {
			e.preventDefault();
			slider.go('prev');
	});
	$('#slider-pages a').click(function(e){
	   e.preventDefault();
	   var ira = $(this).attr("rel");
	   slider.go(ira);
	});

	/*********************/

	$("#play").click(function(e){
		e.preventDefault();
		slider.play();
	});

});